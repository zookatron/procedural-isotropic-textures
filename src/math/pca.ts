/**
 * Compute the eigenvectors and eigenvalues of a given matrix.
 *
 * @param {Array.<number[]>} matrix - A matrix of real numbers
 * @return {{eigenvectors: Array.<number[]>, eigenvalues: number[]}} The eigenvalues and eigenvectors
 */
function getEigenVectors(matrix: number[][]) {
	const height = matrix.length;
	const width = matrix[0].length;
	const invHeight = 1 / height;
	const invWidth = 1 / width;

	const copy: number[][] = new Array(height);
	const average: number[] = new Array(width).fill(0);
	const variance: number[][] = new Array(width);

	// Copy the data to avoid changing source values
	for (let row = 0; row < height; row++) {
		copy[row] = new Array(width);
		for (let col = 0; col < width; col++) {
			copy[row][col] = matrix[row][col];
		}
	}

	// Subtract the mean to center data and convert to deviation
	for (let row = 0; row < height; row++) {
		for (let col = 0; col < width; col++) {
			average[col] += copy[row][col];
		}
	}
	for (let col = 0; col < width; col++) {
		average[col] *= invHeight;
	}
	for (let row = 0; row < height; row++) {
		for (let col = 0; col < width; col++) {
			copy[row][col] -= average[col];
		}
	}

	// Computes variance from deviation
	for (let row = 0; row < width; row++) {
		variance[row] = new Array(width).fill(0);
		for (let col = 0; col < width; col++) {
			for (let innerrow = 0; innerrow < height; innerrow++) {
				variance[row][col] += copy[innerrow][row] * copy[innerrow][col];
			}
		}
	}

	// Computes variance/covariance using population
	for (let row = 0; row < width; row++) {
		for (let col = 0; col < width; col++) {
			variance[row][col] *= invWidth;
		}
	}

	return singularValueDecomposition(variance);
}

/**
 * Compute the thin SVD from G. H. Golub and C. Reinsch, Numer. Math. 14, 403-420 (1970)
 * From the Numeric JS Implementation Copyright (C) 2011 by Sébastien Loisel
 * The C implementation from which this has been taken may be found here: http://www.public.iastate.edu/~dicook/JSS/paper/code/svd.c
 *
 * @param {Array.<number[]>} matrix - A matrix representation of the variance of the original source matrix
 * @return {{eigenvectors: Array.<number[]>, eigenvalues: number[]}} The eigenvalues and eigenvectors
 */
function singularValueDecomposition(matrix: number[][]) {
	const precision = Math.pow(2, -52); // assumes double precision
	const tolerance = 1.e-64 / precision;
	const itmax = 50;
	const height = matrix.length;
	const width = matrix[0].length;

	if (height < width) {
		throw new Error('Need more rows than columns');
	}

	const eigenCompliments: number[] = new Array(width);
	const eigenvalues: number[] = new Array(width);
	for (let col = 0; col < width; col++) {
		eigenCompliments[col] = eigenvalues[col] = 0;
	}

	const leftSingularVectors: number[][] = new Array(height);
	for (let row = 0; row < height; row++) {
		leftSingularVectors[row] = new Array(width);
		for (let col = 0; col < width; col++) {
			leftSingularVectors[row][col] = matrix[row][col];
		}
	}

	const rightSingularVectors: number[][] = new Array(width);
	for (let row = 0; row < width; row++) {
		rightSingularVectors[row] = new Array(width);
		for (let col = 0; col < width; col++) {
			rightSingularVectors[row][col] = 0;
		}
	}

	function pythag(a: number, b: number) {
		a = Math.abs(a);
		b = Math.abs(b);
		if (a > b) {
			return a * Math.sqrt(1 + (b * b / a / a));
		} else if (b === 0) {
			return a;
		}
		return b * Math.sqrt(1 + (a * a / b / b));
	}

	// Householder's reduction to bidiagonal form
	let max = 0.0;
	let reduc = 0;
	for (let col = 0; col < width; col++) {
		eigenCompliments[col] = reduc;
		let sum = 0.0;
		const l = col + 1;
		for (let row = col; row < height; row++) {
			sum += (leftSingularVectors[row][col] * leftSingularVectors[row][col]);
		}
		if (sum <= tolerance) {
			reduc = 0.0;
		} else {
			let f = leftSingularVectors[col][col];
			reduc = Math.sqrt(sum);
			if (f >= 0.0) {
				reduc = -reduc;
			}
			const h = f * reduc - sum;
			leftSingularVectors[col][col] = f - reduc;
			for (let j = l; j < width; j++) {
				sum = 0.0;
				for (let k = col; k < height; k++) {
					sum += leftSingularVectors[k][col] * leftSingularVectors[k][j];
				}
				f = sum / h;
				for (let k = col; k < height; k++) {
					leftSingularVectors[k][j] += f * leftSingularVectors[k][col];
				}
			}
		}
		eigenvalues[col] = reduc;
		sum = 0.0;
		for (let j = l; j < width; j++) {
			sum = sum + leftSingularVectors[col][j] * leftSingularVectors[col][j];
		}
		if (sum <= tolerance) {
			reduc = 0.0;
		} else {
			const f = leftSingularVectors[col][col + 1];
			reduc = Math.sqrt(sum);
			if (f >= 0.0) {
				reduc = -reduc;
			}
			const h = f * reduc - sum;
			leftSingularVectors[col][col + 1] = f - reduc;
			for (let j = l; j < width; j++) {
				eigenCompliments[j] = leftSingularVectors[col][j] / h;
			}
			for (let j = l; j < height; j++) {
				sum = 0.0;
				for (let k = l; k < width; k++) {
					sum += (leftSingularVectors[j][k] * leftSingularVectors[col][k]);
				}
				for (let k = l; k < width; k++) {
					leftSingularVectors[j][k] += sum * eigenCompliments[k];
				}
			}
		}
		const current = Math.abs(eigenvalues[col]) + Math.abs(eigenCompliments[col]);
		if (current > max) {
			max = current;
		}
	}

	// Accumulation of right hand transformations
	let lastCol = 0;
	let lastColVal = 0;
	for (let col = width - 1; col !== -1; col -= 1) {
		if (lastColVal !== 0.0) {
			const h = lastColVal * leftSingularVectors[col][col + 1];
			for (let j = lastCol; j < width; j++) {
				rightSingularVectors[j][col] = leftSingularVectors[col][j] / h;
			}
			for (let j = lastCol; j < width; j++) {
				let sum = 0.0;
				for (let k = lastCol; k < width; k++) {
					sum += leftSingularVectors[col][k] * rightSingularVectors[k][j];
				}
				for (let k = lastCol; k < width; k++) {
					rightSingularVectors[k][j] += (sum * rightSingularVectors[k][col]);
				}
			}
		}
		for (let j = lastCol; j < width; j++) {
			rightSingularVectors[col][j] = 0;
			rightSingularVectors[j][col] = 0;
		}
		rightSingularVectors[col][col] = 1;
		lastColVal = eigenCompliments[col];
		lastCol = col;
	}

	// Accumulation of left hand transformations
	let nextCol = 0;
	for (let col = width - 1; col !== -1; col -= 1) {
		nextCol = col + 1;
		const g = eigenvalues[col];
		for (let j = nextCol; j < width; j++) {
			leftSingularVectors[col][j] = 0;
		}
		if (g !== 0.0) {
			const h = leftSingularVectors[col][col] * g;
			for (let j = nextCol; j < width; j++) {
				let sum = 0.0;
				for (let k = nextCol; k < height; k++) {
					sum += leftSingularVectors[k][col] * leftSingularVectors[k][j];
				}
				const f = sum / h;
				for (let k = col; k < height; k++) {
					leftSingularVectors[k][j] += f * leftSingularVectors[k][col];
				}
			}
			for (let j = col; j < height; j++) {
				leftSingularVectors[j][col] = leftSingularVectors[j][col] / g;
			}
		} else {
			for (let j = col; j < height; j++) {
				leftSingularVectors[j][col] = 0;
			}
		}
		leftSingularVectors[col][col] += 1;
	}

	// Diagonalization of the bidiagonal form
	const xPrecision = precision * max;
	let conv = 0;
	for (let col = width - 1; col !== -1; col -= 1) {
		for (let iteration = 0; iteration < itmax; iteration++) { // Test splitting
			let testConvergence = false;
			for (conv = col; conv !== -1; conv += -1) {
				if (Math.abs(eigenCompliments[conv]) <= xPrecision) {
					testConvergence = true;
					break;
				}
				if (Math.abs(eigenvalues[conv - 1]) <= xPrecision) {
					break;
				}
			}
			if (!testConvergence) { // Cancellation of eigenCompliments[conv] if conv>0
				let innerC = 0.0;
				let innerS = 1.0;
				const l1 = conv - 1;
				for (let i = conv; i < col + 1; i++) {
					const innerF = innerS * eigenCompliments[i];
					eigenCompliments[i] = innerC * eigenCompliments[i];
					if (Math.abs(innerF) <= xPrecision) {
						break;
					}
					const innerG = eigenvalues[i];
					const h = pythag(innerF, innerG);
					eigenvalues[i] = h;
					innerC = innerG / h;
					innerS = -innerF / h;
					for (let j = 0; j < height; j++) {
						const innerY = leftSingularVectors[j][l1];
						const innerZ = leftSingularVectors[j][i];
						leftSingularVectors[j][l1] = innerY * innerC + (innerZ * innerS);
						leftSingularVectors[j][i] = -innerY * innerS + (innerZ * innerC);
					}
				}
			}
			// Test convergence
			let z = eigenvalues[col];
			if (conv === col) { // Convergence
				if (z < 0.0) { // eigenvalues[col] is made non-negative
					eigenvalues[col] = -z;
					for (let j = 0; j < width; j++) {
						rightSingularVectors[j][col] = -rightSingularVectors[j][col];
					}
				}
				break; // Break out of iteration loop and move on to next col value
			}
			if (iteration >= itmax - 1) {
				throw new Error('Error: no convergence.');
			}
			// Shift from bottom 2x2 minor
			let x = eigenvalues[conv];
			let y = eigenvalues[col - 1];
			let outerG = eigenCompliments[col - 1];
			const outerH = eigenCompliments[col];
			let f = ((y - z) * (y + z) + (outerG - outerH) * (outerG + outerH)) / (2.0 * outerH * y);
			outerG = pythag(f, 1.0);
			if (f < 0.0) {
				f = ((x - z) * (x + z) + outerH * (y / (f - outerG) - outerH)) / x;
			} else {
				f = ((x - z) * (x + z) + outerH * (y / (f + outerG) - outerH)) / x;
			}
			// Next QR transformation
			let c = 1.0;
			let s = 1.0;
			for (let i = conv + 1; i < col + 1; i++) {
				let g = eigenCompliments[i];
				y = eigenvalues[i];
				let h = s * g;
				g = c * g;
				z = pythag(f, h);
				eigenCompliments[i - 1] = z;
				c = f / z;
				s = h / z;
				f = x * c + g * s;
				g = -x * s + g * c;
				h = y * s;
				y = y * c;
				for (let j = 0; j < width; j++) {
					x = rightSingularVectors[j][i - 1];
					z = rightSingularVectors[j][i];
					rightSingularVectors[j][i - 1] = x * c + z * s;
					rightSingularVectors[j][i] = -x * s + z * c;
				}
				z = pythag(f, h);
				eigenvalues[i - 1] = z;
				c = f / z;
				s = h / z;
				f = c * g + s * y;
				x = -s * g + c * y;
				for (let j = 0; j < height; j++) {
					y = leftSingularVectors[j][i - 1];
					z = leftSingularVectors[j][i];
					leftSingularVectors[j][i - 1] = y * c + z * s;
					leftSingularVectors[j][i] = -y * s + z * c;
				}
			}
			eigenCompliments[conv] = 0.0;
			eigenCompliments[col] = f;
			eigenvalues[col] = x;
		}
	}

	// Round down very small eigenvalues
	for (let index = 0; index < eigenvalues.length; index++) {
		if (eigenvalues[index] < xPrecision) {
			eigenvalues[index] = 0;
		}
	}

	// Sort eigenvalues
	for (let col = 0; col < width; col++) {
		for (let othercol = col - 1; othercol >= 0; othercol--) {
			if (eigenvalues[othercol] < eigenvalues[col]) {
				let temp = eigenvalues[othercol];
				eigenvalues[othercol] = eigenvalues[col];
				eigenvalues[col] = temp;
				for (let index = 0; index < leftSingularVectors.length; index++) {
					temp = leftSingularVectors[index][col];
					leftSingularVectors[index][col] = leftSingularVectors[index][othercol];
					leftSingularVectors[index][othercol] = temp;
				}
				for (let index = 0; index < rightSingularVectors.length; index++) {
					temp = rightSingularVectors[index][col];
					rightSingularVectors[index][col] = rightSingularVectors[index][othercol];
					rightSingularVectors[index][othercol] = temp;
				}
				col = othercol;
			}
		}
	}

	const eigenvectors: number[][] = new Array(width);

	for (let row = 0; row < width; row++) {
		eigenvectors[row] = new Array(height);
		for (let col = 0; col < height; col++) {
			eigenvectors[row][col] = -leftSingularVectors[col][row];
		}
	}

	return {eigenvectors, eigenvalues};
}

export default getEigenVectors;
