
export default class Complex {
	public real: number;
	public imaginary: number;

	constructor(real?: number, imaginary?: number) {
		this.real = real === undefined ? 0 : real;
		this.imaginary = imaginary === undefined ? 0 : imaginary;
	}

	public copy(other: Complex) {
		this.real = other.real;
		this.imaginary = other.imaginary;
		return this;
	}

	public clone() {
		return new Complex(this.real, this.imaginary);
	}

	public set(real: number, imaginary: number) {
		this.real = real;
		this.imaginary = imaginary;
		return this;
	}

	public magnitudeSquared() {
		return this.real * this.real + this.imaginary * this.imaginary;
	}

	public magnitude() {
		return Math.sqrt(this.magnitudeSquared());
	}

	public plus(other: Complex) {
		this.real += other.real;
		this.imaginary += other.imaginary;
		return this;
	}

	public minus(other: Complex) {
		this.real -= other.real;
		this.imaginary -= other.imaginary;
		return this;
	}

	public scale(amount: number) {
		this.real *= amount;
		this.imaginary *= amount;
		return this;
	}

	public times(other: Complex) {
		const real = this.real * other.real - this.imaginary * other.imaginary;
		const imaginary = this.real * other.imaginary + this.imaginary * other.real;
		this.real = real;
		this.imaginary = imaginary;
		return this;
	}

	public serialize() {
		return {
			real: this.real,
			imaginary: this.imaginary,
		};
	}

	public deserialize(data: ReturnType<Complex['serialize']>) {
		this.real = data.real;
		this.imaginary = data.imaginary;
		return this;
	}
}
