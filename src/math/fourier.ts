import {memoize} from 'lodash-es';
import Complex from './complex';

const savedRotator = new Complex();
const savedFirst = new Complex();
const savedSecond = new Complex();
const getTempFrequency = memoize((size: number) => new Array(size).fill(null).map(() => new Complex()));
const getComplexSignal = memoize((size: number) => new Array(size).fill(null).map(() => new Complex()));
const getRotation = memoize((angle: number) => [Math.cos(angle), Math.sin(angle)] as const);
const rotation = (angle: number) => savedRotator.set(...getRotation(angle));

function recursiveFourierTransform2D(signal: number[], frequency: Complex[], signalPos: number, frequencyPos: number, amount: number, step: number) {
	if (amount === 1) {
		frequency[frequencyPos].set(signal[signalPos], 0);
	} else {
		recursiveFourierTransform2D(signal, frequency, signalPos, frequencyPos, amount / 2, 2 * step);
		recursiveFourierTransform2D(signal, frequency, signalPos + step, frequencyPos + amount / 2, amount / 2, 2 * step);

		for (let position = 0; position < amount / 2; position++) {
			const first = frequency[frequencyPos + position];
			const second = frequency[frequencyPos + position + amount / 2];
			savedFirst.copy(first);
			savedSecond.copy(second);
			const rotator = rotation(-2 * Math.PI * position / amount);
			first.copy(rotator).times(savedSecond).plus(savedFirst);
			second.copy(rotator).times(savedSecond).minus(savedFirst).scale(-1);
		}
	}
}

function recursiveInverseFourierTransform2D(signal: Complex[], frequency: Complex[], signalPos: number, frequencyPos: number, amount: number, step: number) {
	if (amount === 1) {
		signal[signalPos].copy(frequency[frequencyPos]);
	} else {
		recursiveInverseFourierTransform2D(signal, frequency, signalPos, frequencyPos, amount / 2, 2 * step);
		recursiveInverseFourierTransform2D(signal, frequency, signalPos + amount / 2, frequencyPos + step, amount / 2, 2 * step);
		for (let position = 0; position < amount / 2; position++) {
			const first = signal[signalPos + position];
			const second = signal[signalPos + position + amount / 2];
			savedFirst.copy(first);
			savedSecond.copy(second);
			const rotator = rotation(2 * Math.PI * position / amount);
			first.copy(rotator).times(savedSecond).plus(savedFirst);
			second.copy(rotator).times(savedSecond).minus(savedFirst).scale(-1);
		}
	}
}

function shiftFourierTransform2D(values: Complex[], size: number) {
	const halfSize = Math.floor(size / 2);
	for (let y = 0; y < halfSize; y++) {
		for (let x = 0; x < size; x++) {
			const newX = (x + halfSize) % size;
			const newY = (y + halfSize) % size;
			const index = y * size + x;
			const newIndex = newY * size + newX;
			const temp = values[newIndex];
			values[newIndex] = values[index];
			values[index] = temp;
		}
	}
}

export function FourierTransform2D(size: number, signal: number[], frequency: Complex[]) {
	recursiveFourierTransform2D(signal, frequency, 0, 0, signal.length, 1);
	shiftFourierTransform2D(frequency, size);
}

export function InverseFourierTransform2D(size: number, frequency: Complex[], signal: number[]) {
	const complexSignal = getComplexSignal(signal.length);
	const tempFrequency = getTempFrequency(frequency.length);
	for (let value = 0; value < frequency.length; value++) {
		tempFrequency[value].copy(frequency[value]);
	}

	shiftFourierTransform2D(tempFrequency, size);
	recursiveInverseFourierTransform2D(complexSignal, tempFrequency, 0, 0, tempFrequency.length, 1);

	for (let value = 0; value < signal.length; value++) {
		signal[value] = complexSignal[value].real / signal.length;
	}
}
