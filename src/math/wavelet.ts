const TWOPOWMMINUSTHIRTYTWO = 2.3283064365386963e-10;
const TWOPOWTHIRTYTWO = 0x100000000;

// https://github.com/LRFLEW/NoiseGame/blob/master/noiseFrag.glsl
class Hash {
	public number(value: number, seed: number): number {
		let hash = seed;
		hash = this.subhash(value, hash);
		return this.finalize(hash);
	}

	private subhash(value: number, hash: number): number {
		value = Math.imul(value, 0x5bd1e995);
		value ^= value >>> 24;
		value = Math.imul(value, 0x5bd1e995);
		hash = Math.imul(hash, 0x5bd1e995) ^ value;
		return hash;
	}

	private finalize(hash: number): number {
		hash ^= hash >>> 13;
		hash = Math.imul(hash, 0x5bd1e995);
		hash ^= hash >>> 15;
		return hash >>> 0;
	}
}

const hash = new Hash();

class GeneratorContext {
	public c: number;
	public s0: number;
	public s1: number;
	public s2: number;

	constructor(seed: number) {
		this.c = 1;
		this.s0 = hash.number(seed, 1305169) * TWOPOWMMINUSTHIRTYTWO;
		this.s1 = hash.number(seed, 15503687) * TWOPOWMMINUSTHIRTYTWO;
		this.s2 = hash.number(seed, 179426407) * TWOPOWMMINUSTHIRTYTWO;
		return this;
	}
}

class Generator {
	private defaultContext: GeneratorContext | undefined;

	public context(seed: number): GeneratorContext {
		return new GeneratorContext(seed);
	}

	public float(context?: GeneratorContext): number {
		if (!context) {
			if (!this.defaultContext) {
				this.defaultContext = this.context(TWOPOWTHIRTYTWO);
			}
			context = this.defaultContext;
		}
		const t = 2091639 * context.s0 + context.c * TWOPOWMMINUSTHIRTYTWO;
		context.s0 = context.s1;
		context.s1 = context.s2;
		context.c = t >>> 0;
		context.s2 = t - context.c;
		return context.s2;
	}

	public integer(context?: GeneratorContext): number {
		return this.float(context) * TWOPOWTHIRTYTWO >>> 0;
	}
}

const generator = new Generator();

const gaussianCoefficients = [
	0.000334, -0.001528, 0.000410, 0.003545, -0.000938, -0.008233, 0.002172, 0.019120,
	-0.005040, -0.044412, 0.011655, 0.103311, -0.025936, -0.243780, 0.033979, 0.655340,
];

function Downsample(from: number[], fromStart: number, to: number[], toStart: number, size: number, stride: number) {
	const halfSize = size / 2;
	for (let index = 0; index < halfSize; index++) {
		let value = 0;
		for (let coefficient = 0; coefficient < 16; coefficient++) {
			const fromValue =
				from[fromStart + Wrap(2 * index - 16 + coefficient, size) * stride] +
				from[fromStart + Wrap(2 * index + 15 - coefficient, size) * stride];
			value += gaussianCoefficients[coefficient] * fromValue;
		}
		to[toStart + index * stride] = value;
	}
}

function Upsample(from: number[], fromStart: number, to: number[], toStart: number, size: number, stride: number) {
	const halfSize = size / 2;
	for (let index = 0; index < size; index++) {
		const oddness = index % 2;
		const half = (index - oddness) / 2;

		to[toStart + index * stride] =
			(oddness ? 0.25 : 0.75) * from[fromStart + Wrap(half, halfSize) * stride] +
			(oddness ? 0.75 : 0.25) * from[fromStart + Wrap(half + 1, halfSize) * stride];
	}
}

function Wrap(value: number, max: number): number {
	const result = value % max;
	return result < 0 ? result + max : result;
}

export default class WaveletNoise {
	private coefficients: number[];
	private size: number;

	constructor(size: number, seed = 1) {
		// tile size must be even
		const finalSize = size % 2 ? size + 1 : size;
		this.size = finalSize;
		const length = finalSize * finalSize;

		const temp1: number[] = new Array(length).fill(0);
		const temp2: number[] = new Array(length).fill(0);
		const noise: number[] = new Array(length).fill(0);

		const context = generator.context(seed);

		// Fill the tile with random numbers in the range -1 to 1.
		for (let index = 0; index < length; index++) {
			noise[index] = generator.float(context) * 2 - 1;
		}

		// Downsample and upsample the tile, first in the x dimension and then in the y dimension
		for (let y = 0; y < finalSize; y++) {
			const row = y * finalSize;
			Downsample(noise, row, temp1, row, finalSize, 1);
			Upsample(temp1, row, temp2, row, finalSize, 1);
		}

		for (let x = 0; x < finalSize; x++) {
			const column = x;
			Downsample(temp2, column, temp1, column, finalSize, finalSize);
			Upsample(temp1, column, temp2, column, finalSize, finalSize);
		}

		// Subtract out the coarse-scale contribution
		for (let i = 0; i < length; i++) {
			noise[i] -= temp2[i];
		}

		// Avoid even/odd variance difference by adding odd-offset version of noise to itself.
		const middle = finalSize / 2;
		const offset = middle % 2 ? middle : middle + 1;
		let index = 0;
		for (let x = 0; x < finalSize; x++) {
			for (let y = 0; y < finalSize; y++) {
				temp1[index++] = noise[Wrap(x + offset, finalSize) + Wrap(y + offset, finalSize) * finalSize];
			}
		}
		for (let index = 0; index < length; index++) {
			noise[index] += temp1[index];
		}

		this.coefficients = noise;
	}

	public get(x: number, y: number): number {
		const size = this.size;
		const coefficients = this.coefficients;

		// Evaluate quadratic B-spline basis functions
		const xOffset = x - 0.5;
		const xMid = Math.ceil(xOffset);
		const xPosition = xMid - xOffset;
		const xMinusPosition = 1 - xPosition;
		const xWeightPrev = xPosition * xPosition * 0.5;
		const xWeightNext = xMinusPosition * xMinusPosition * 0.5;
		const xWeightMid = 1 - xWeightPrev - xWeightNext;
		const yOffset = y - 0.5;
		const yMid = Math.ceil(yOffset);
		const yPosition = yMid - yOffset;
		const yMinusPosition = 1 - yPosition;
		const yWeightPrev = yPosition * yPosition * 0.5;
		const yWeightNext = yMinusPosition * yMinusPosition * 0.5;
		const yWeightMid = 1 - yWeightPrev - yWeightNext;

		// Evaluate noise by weighting noise coefficients by basis function values
		const xCoefficientPrev = Wrap(xMid - 1, size);
		const xCoefficientMid = Wrap(xMid, size);
		const xCoefficientNext = Wrap(xMid + 1, size);
		const yCoefficientPrev = Wrap(yMid - 1, size);
		const yCoefficientMid = Wrap(yMid, size);
		const yCoefficientNext = Wrap(yMid + 1, size);

		const result =
			xWeightPrev * yWeightPrev * coefficients[xCoefficientPrev + yCoefficientPrev * size] +
			xWeightMid * yWeightPrev * coefficients[xCoefficientMid + yCoefficientPrev * size] +
			xWeightNext * yWeightPrev * coefficients[xCoefficientNext + yCoefficientPrev * size] +
			xWeightPrev * yWeightMid * coefficients[xCoefficientPrev + yCoefficientMid * size] +
			xWeightMid * yWeightMid * coefficients[xCoefficientMid + yCoefficientMid * size] +
			xWeightNext * yWeightMid * coefficients[xCoefficientNext + yCoefficientMid * size] +
			xWeightPrev * yWeightNext * coefficients[xCoefficientPrev + yCoefficientNext * size] +
			xWeightMid * yWeightNext * coefficients[xCoefficientMid + yCoefficientNext * size] +
			xWeightNext * yWeightNext * coefficients[xCoefficientNext + yCoefficientNext * size];

		return result > 1 ? 1 : result < -1 ? -1 : result;
	}
}
