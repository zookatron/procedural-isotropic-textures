
export const NUMBER_OF_BANDS = 7;
export const SIZE = Math.pow(2, NUMBER_OF_BANDS + 1);
export const SIMPLEX_VARIANCE = 0.3 * 0.3;
