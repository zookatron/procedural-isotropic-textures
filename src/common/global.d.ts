declare module '*.css' {
	const classes: { [key: string]: string };
	export default classes;
}

declare module '*.png' {
	const url: string;
	export default url;
}

declare module 'worker-loader!*' {
	class WebpackWorker extends Worker {
		constructor();
	}

	export default WebpackWorker;
}
