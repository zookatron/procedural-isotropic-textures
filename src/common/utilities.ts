
export async function readFile(file: File) {
	return await new Promise<string>((resolve, reject) => {
		const reader = new FileReader();
		reader.addEventListener('abort', () => {
			reject(new Error('FileReader load aborted!'));
		});
		reader.addEventListener('error', () => {
			reject(reader.error);
		});
		reader.addEventListener('load', () => {
			if (typeof reader.result === 'string') {
				resolve(reader.result);
			} else {
				reject(new Error('FileReader failed to load data URL properly!'));
			}
		});
		reader.readAsDataURL(file);
	});
}

export async function loadImage(url: string, size: number) {
	const image = await new Promise<HTMLImageElement>((resolve, reject) => {
		const image = new Image();
		image.addEventListener('load', () => resolve(image));
		image.addEventListener('error', () => reject(new Error(`Unable to load image "${url}"!`)));
		image.src = url;
	});
	const canvas = document.createElement('canvas');
	canvas.width = size;
	canvas.height = size;
	const context = canvas.getContext('2d');
	if (!context) {
		throw new Error('Unable to get canvas context!');
	}
	context.imageSmoothingEnabled = true;
	context.imageSmoothingQuality = 'high';
	context.drawImage(await createImageBitmap(image), 0, 0, image.width, image.height, 0, 0, size, size);
	return context.getImageData(0, 0, size, size);
}

export function averageImages(images: ImageData[]) {
	const width = images[0].width;
	const height = images[0].height;
	const data = new Uint8ClampedArray(width * height * 4);

	for (let y = 0; y < height; y++) {
		for (let x = 0; x < width; x++) {
			const pixel = y * width + x;
			let r = 0, g = 0, b = 0, a = 0;
			for (let image = images.length - 1; image >= 0; image--) {
				r += images[image].data[pixel * 4 + 0];
				g += images[image].data[pixel * 4 + 1];
				b += images[image].data[pixel * 4 + 2];
				a += images[image].data[pixel * 4 + 3];
			}
			data[pixel * 4 + 0] = r / images.length;
			data[pixel * 4 + 1] = g / images.length;
			data[pixel * 4 + 2] = b / images.length;
			data[pixel * 4 + 3] = a / images.length;
		}
	}

	return new ImageData(data, width, height);
}

export class Timer {
	private startTime = 0;
	private endTime = 0;

	public start() {
		this.startTime = Date.now();
	}

	public end(name?: string) {
		this.endTime = Date.now();
		if (name) {
			this.show(name);
		}
	}

	public show(name: string) {
		console.log(`${name}: ${this.duration().toFixed(3)}`);
	}

	public duration() {
		return (this.endTime - this.startTime) / 1000;
	}
}
