import React, {useRef, useEffect} from 'react';
import {WebGLRenderer, PerspectiveCamera, Scene, Mesh, MeshBasicMaterial, SphereBufferGeometry, Color, Vector3, ArrowHelper} from 'three';
import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls';
import styles from '../styles/pca-plot.css';

export function PCAPlot({center, axes, image, width, height}: {
	center: number[] | null, axes: number[][] | null, image: {x: number[], y: number[], z: number[]} | null, width: number, height: number
}) {
	const container = useRef<HTMLCanvasElement>(null);

	useEffect(() => {
		if (!center || !axes || !image) {
			return;
		}
		if (!container.current) {
			throw new Error('No container reference available!');
		}

		const renderer = new WebGLRenderer({canvas: container.current, antialias: true});
		renderer.setSize(width, height);
		renderer.setPixelRatio(window.devicePixelRatio);
		renderer.setClearColor(new Color(0, 0, 0));

		const scene = new Scene();
		scene.add(new ArrowHelper(new Vector3(1, 0, 0), new Vector3(0, 0, 0), 1, 0xff0000));
		scene.add(new ArrowHelper(new Vector3(0, 1, 0), new Vector3(0, 0, 0), 1, 0x00ff00));
		scene.add(new ArrowHelper(new Vector3(0, 0, 1), new Vector3(0, 0, 0), 1, 0x0000ff));

		const camera = new PerspectiveCamera(30, width / height, 1, 1000);
		camera.position.set(0, 2, 4);
		camera.lookAt(scene.position);
		camera.rotation.reorder('YXZ');
		camera.updateProjectionMatrix();

		const controls = new OrbitControls(camera, renderer.domElement);
		controls.enableZoom = false;

		let animationFrame: number;
		const frame = () => {
			animationFrame = requestAnimationFrame(frame);
			controls.update();
			renderer.render(scene, camera);
		};
		animationFrame = requestAnimationFrame(frame);

		const pixelGeo = new SphereBufferGeometry(0.01, 2, 2);

		for (let index = 0; index < image.x.length; index++) {
			const r = image.x[index];
			const g = image.y[index];
			const b = image.z[index];
			const pixel = new Mesh(pixelGeo, new MeshBasicMaterial({color: new Color(r, g, b)}));
			pixel.frustumCulled = false;
			pixel.position.set(r, g, b);
			scene.add(pixel);
		}

		const centerVector = new Vector3(center[0], center[1], center[2]).multiplyScalar(0.5).addScalar(0.5);
		scene.add(new ArrowHelper(new Vector3(axes[0][0], axes[0][1], axes[0][2]).normalize(), centerVector, 0.5, 0xff0000));
		scene.add(new ArrowHelper(new Vector3(axes[1][0], axes[1][1], axes[1][2]).normalize(), centerVector, 0.5, 0x00ff00));
		scene.add(new ArrowHelper(new Vector3(axes[2][0], axes[2][1], axes[2][2]).normalize(), centerVector, 0.5, 0x0000ff));

		return () => {
			cancelAnimationFrame(animationFrame);
			controls.dispose();
			renderer.dispose();
			controls.dispose();
			pixelGeo.dispose();
		};
	}, [center, axes, image, container.current]);

	return <canvas ref={container} className={styles.pcaPlot} />;
}
