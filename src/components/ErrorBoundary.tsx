import React, {PureComponent} from 'react';
import styles from '../styles/error-boundary.css';

export class ErrorBoundary extends PureComponent<{}, { error: Error | false }> {
	constructor(props: {}) {
		super(props);
		this.state = {error: false};
	}

	static getDerivedStateFromError(error: Error) {
		return {error};
	}

	reload() {
		window.location.reload();
	}

	render() {
		if (this.state.error) {
			return <div className={styles.errorBoundary}>
				<h1>Uh oh! Something went wrong!</h1>
				<p>
					The extension has encountered an error it was not able to recover from. Please&nbsp;
					<a href="https://gitlab.com/zookatron/procedural-isotropic-textures/-/issues" target="_blank" rel="noreferrer">
					send the following data to the developers</a> along with a description of what you were
					doing at the time of the error:
				</p>
				<textarea rows={ 10 } cols={ 100 } value={ `${this.state.error.message}\n${this.state.error.stack}` } readOnly={ true } /><br />
				<button onClick={ this.reload }>Reload Page</button>
			</div>;
		} else {
			return this.props.children;
		}
	}
}
