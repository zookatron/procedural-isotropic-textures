import React, {useRef, useEffect, useState} from 'react';
import classnames from 'classnames';
import styles from '../styles/graph.css';

export function GraphSet({selectable, children}: { selectable?: boolean, children: React.ReactNode }) {
	return <div className={classnames(styles.graphSet, selectable && styles.selectable)}>
		{children}
	</div>;
}

interface GraphProps {
	label: string,
	className?: string,
	loading?: boolean,
	selected?: boolean,
	onSelect?: () => unknown,
}

export function Graph({children, label, className, loading, selected, onSelect}: { children: React.ReactNode } & GraphProps) {
	return <div className={classnames(className, styles.graph, loading && styles.loading, selected && styles.selected)} onClick={onSelect}>
		<div className={styles.graphLabel}>{label}</div>
		<div className={styles.graphContent}>
			{children}
		</div>
	</div>;
}

export function ImageGraph({image, label, className, selected, onSelect}: {image: string | ImageData | null | undefined} & GraphProps) {
	const canvas = useRef<HTMLCanvasElement>(null);
	const [drawn, setDrawn] = useState(false);

	useEffect(() => void (async () => {
		setDrawn(false);
		if(typeof image === 'string') {
			setDrawn(true);
			return;
		}
		if (!image) {
			return;
		}
		if (!canvas.current) {
			throw new Error('No canvas reference available!');
		}
		const context = canvas.current.getContext('2d');
		if (!context) {
			throw new Error('Getting canvas context failed!');
		}
		context.drawImage(await createImageBitmap(image), 0, 0);
		setDrawn(true);
	})(), [image, canvas.current]);

	return <Graph className={className} label={label} loading={!image || !drawn} selected={selected} onSelect={onSelect}>
		{ typeof image === 'string' && <img src={image} /> }
		{ typeof image !== 'string' && <canvas ref={canvas} width={image?.width} height={image?.height} /> }
	</Graph>;
}
