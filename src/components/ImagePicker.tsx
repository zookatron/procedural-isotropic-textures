import {memoize} from 'lodash-es';
import React, {useState, useCallback} from 'react';
import styles from '../styles/image-picker.css';
import classnames from 'classnames';
import {readFile} from '../common/utilities';
import concrete from '../images/concrete.png';
import dirt from '../images/dirt.png';
import fire from '../images/fire.png';
import grass from '../images/grass.png';
import greenrock from '../images/greenrock.png';
import jeans from '../images/jeans.png';
import marble1 from '../images/marble1.png';
import marble2 from '../images/marble2.png';
import marble3 from '../images/marble3.png';
import rocksalt from '../images/rocksalt.png';
import rustymetal from '../images/rustymetal.png';
import stone from '../images/stone.png';
import towel from '../images/towel.png';
import vinylflooring from '../images/vinylflooring.png';

const images = [
	concrete,
	dirt,
	fire,
	grass,
	greenrock,
	jeans,
	marble1,
	marble2,
	marble3,
	rocksalt,
	rustymetal,
	stone,
	towel,
	vinylflooring,
];

export function ImagePicker({onChange}: { onChange: (image: string) => unknown }) {
	const [selected, setSelected] = useState('');
	const [uploadedImage, setUploadedImage] = useState('');

	const select = useCallback(memoize((image: string) => () => {
		setSelected(image);
		onChange(image);
	}), [setSelected, onChange]);

	const upload = useCallback(async (event: React.ChangeEvent<HTMLInputElement>) => {
		if (!event.target.files) {
			return;
		}
		const image = await readFile(event.target.files![0]);
		setUploadedImage(image);
		setSelected(image);
		onChange(image);
	}, [setUploadedImage, setSelected, onChange]);

	return <div className={styles.imagePicker}>
		{ images.map(image => <div key={image} className={classnames(styles.image, selected === image && styles.selected)} onClick={select(image)}>
			<img src={image} />
		</div>) }
		<div className={classnames(styles.image, uploadedImage && selected === uploadedImage && styles.selected)}>
			{!uploadedImage && <label className={styles.upload}>
				Upload Image
				<input type="file" onChange={upload} />
			</label>}
			{uploadedImage && <img src={uploadedImage} onClick={select(uploadedImage)} />}
		</div>
	</div>;
}
