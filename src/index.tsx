import {memoize, mapValues, range, keys, set} from 'lodash-es';
import React, {useState, useCallback} from 'react';
import ReactDOM from 'react-dom';
import {ErrorBoundary} from './components/ErrorBoundary';
import {ImagePicker} from './components/ImagePicker';
import {GraphSet, Graph, ImageGraph} from './components/Graph';
import {PCAPlot} from './components/PCAPlot';
import styles from './styles/app.css';
import {NUMBER_OF_BANDS, SIZE} from './common/constants';
import {loadImage} from './common/utilities';
import Worker from 'worker-loader!./worker';
import stone from './images/stone.png';
import generatedstone from './images/generatedstone.png';

type ChannelsOf<Type> = { x: Type, y: Type, z: Type };

export function App() {
	const [data, setData] = useState<{
		image: string,
		worker: Worker,
		originalColor: ImageData | null,
		original: ChannelsOf<ImageData> | null,
		originalPixels: ChannelsOf<number[]> | null,
		correlation: ChannelsOf<ImageData> | null,
		decorrelated: ChannelsOf<ImageData> | null,
		decorrelatedAxes: number[][] | null,
		decorrelatedAxesCenter: number[] | null,
		channels: ChannelsOf<{
			histogram: ImageData | null,
			probability: ImageData | null,
			normalizationMap: ImageData | null,
			normalized: ImageData | null,
			normalizedHistogram: ImageData | null,
			normalizedProbability: ImageData | null,
			frequency: ImageData | null,
			frequencyBands: ImageData | null,
			bandWeights: Array<{ value: number, color: string }> | null,
			noiseLayers: Array<ImageData> | null,
			noiseLayerFrequencies: Array<ImageData> | null,
			noiseLayerWeightedFrequencies: Array<ImageData> | null,
			noise: ImageData | null,
			noiseFrequency: ImageData | null,
			denormalized: ImageData | null,
			denormalizedHistogram: ImageData | null,
		}>,
		colors: ChannelsOf<ImageData> | null,
		final: ImageData | null,
	} | null>(null);

	const [channel, setChannel] = useState<'x' | 'y' | 'z'>('x');

	const selectChannel = useCallback(memoize((newChannel: 'x' | 'y' | 'z') => () => setChannel(newChannel)), [setChannel]);

	const pickImage = useCallback(async (image: string) => {
		if (data && data.worker) {
			data.worker.terminate();
		}
		const worker = new Worker();
		const savedData = {
			image,
			worker,
			originalColor: await loadImage(image, SIZE),
			original: null,
			originalPixels: null,
			correlation: null,
			decorrelated: null,
			decorrelatedAxes: null,
			decorrelatedAxesCenter: null,
			regularHistogram: null,
			regularProbability: null,
			channels: mapValues({x: null, y: null, z: null}, () => ({
				histogram: null,
				probability: null,
				normalizationMap: null,
				normalized: null,
				normalizedHistogram: null,
				normalizedProbability: null,
				frequency: null,
				frequencyBands: null,
				bandWeights: null,
				noiseLayers: null,
				noiseLayerFrequencies: null,
				noiseLayerWeightedFrequencies: null,
				noise: null,
				noiseFrequency: null,
				denormalized: null,
				denormalizedHistogram: null,
			})),
			colors: null,
			final: null,
		};
		setData(savedData);
		worker.addEventListener('message', ({data: newData}) => keys(newData).forEach(key => setData({...set(savedData, key, newData[key])})));
		worker.postMessage({image: savedData.originalColor});
	}, [data, setData]);

	const refreshImage = useCallback(async () => {
		if (!data || !data.image) {
			return;
		}
		pickImage(data.image);
	}, [pickImage, data]);

	return <>
		<header>
			<h1>Procedural Isotropic Stochastic Textures by Example</h1>
			<h5>Research by Ares Lagae, Peter Vangorp, Toon Lenaerts, and Philip Dutr&eacute;</h5>
			<h5>Implementation by <a href="https://zookatron.com/" target="_blank" rel="noreferrer">Tim Zook</a></h5>
		</header>
		<main>
			<section>
				<aside>
					<h3>Introduction</h3>
					<p>
						The paper&nbsp;
						<a href="http://graphics.cs.kuleuven.be/publications/LVLD10PISTE" target="_blank" rel="noreferrer">
						Procedural Isotropic Stochastic Textures by Example by Ares Lagae, Peter Vangorp, Toon Lenaerts, and Philip Dutré
						</a> describes a method for procedurally generating a texture based on a provided sample. This can be useful in computer graphics as
						it allows one to generate a large amount of similar texture data that can cover a large area without repeating the original texture
						which is often quite noticeable to the human eye. The method described in this paper uses synthesis via noise generation rather than
						image stitching. Image stitching is more flexible as it can produce good results on virtually any kind of texture, but it has the
						downside that is generally much slower and requires access to a high-quality version of the original sample and therefore is usually
						generated prior to runtime and saved as an image rather than being generated dynamically. It also has the downside that it can
						sometimes still produce noticeable repetition when generating large amounts of texture as it is limited to sampling from a finite set
						of data. The noise generation approach used in this paper is more limited as it can only produce good results for textures that are
						isotropic, but it is fast enough that it allows one to easily run the generation of the texture at runtime and only requires access to
						a very compact "fingerprint" representation of the original image. It also never generates noticeable repetition in the textures it
						generates because it is based on random noise generation.
					</p>
					<p>
						This application is an interactive explanation of how this algorithm works. Scroll down and choose a source image below to get started.
					</p>
					<GraphSet>
						<ImageGraph label="Source Image" image={stone} />
						<Graph label="">
							<div className={styles.arrow}>
								&#x27A1;
							</div>
						</Graph>
						<ImageGraph label="Generated Texture" image={generatedstone} />
					</GraphSet>
				</aside>
			</section>
			<section>
				<aside>
					<h3>Source Image</h3>
					<p>Choose an image or upload your own to get started:</p>
					<ImagePicker onChange={pickImage} />
				</aside>
			</section>
			{ data && <section>
				<aside>
					<h3>Color Channels</h3>
					<p>
						In computer science, an image is usually represented as a combination of three channels, one for red, green, and blue. These channels,
						labeled R, G, and B, respectively, are combined via&nbsp;
						<a href="https://en.wikipedia.org/wiki/Additive_color" target="_blank" rel="noreferrer">additive color mixing</a>
						&nbsp;to create all possible colors.
					</p>
					<GraphSet>
						<ImageGraph label="Red Channel" image={data.original?.x} />
						<ImageGraph label="Green Channel" image={data.original?.y} />
						<ImageGraph label="Blue Channel" image={data.original?.z} />
					</GraphSet>
				</aside>
			</section> }
			{ data && <section>
				<aside>
					<h3>Channel Correlation</h3>
					<p>
						However, these channels are not conducive to frequency analysis because they are usually highly correlated. This means that variations
						in one channel will usually be accompanied by corresponding changes in another channel. For example, if the image has a large yellow
						area, the red and green channels will be highly correlated in that area because red plus green makes yellow. In the images below,
						white areas correspond to high correlation between the channels and black areas correspond to low correlation between the channels.
					</p>
					<GraphSet>
						<ImageGraph label="Red/Green Correlation" image={data.correlation?.x} />
						<ImageGraph label="Green/Blue Correlation" image={data.correlation?.y} />
						<ImageGraph label="Red/Blue Correlation" image={data.correlation?.z} />
					</GraphSet>
				</aside>
			</section> }
			{ data && <section>
				<aside>
					<h3>Principal Component Analysis</h3>
					<p>
						To solve the issues of the correlated color channels we will run the channels through&nbsp;
						<a href="https://en.wikipedia.org/wiki/Principal_component_analysis" target="_blank" rel="noreferrer">Principal Component Analysis</a>.
						Principal Component Analysis, or PCA, finds a new set of axes that represent colors that vary independently. It is usually impossible
						to find a set of completely uncorrelated axes, but it is still a big improvement over the raw RGB channels. You can see below a 3D plot
						of the colors of each pixel of the source image, and the new set of axes found by PCA in the center.
					</p>
					<PCAPlot center={data.decorrelatedAxesCenter} axes={data.decorrelatedAxes} image={data.originalPixels} width={800} height={600} />
				</aside>
			</section> }
			{ data && <section>
				<aside>
					<h3>Decorrelated Channels</h3>
					<p>
						After PCA we will have three new channels, one for each of the new axes, which we will call X, Y, and Z. We will save the mapping
						between the original RGB axes and the new XYZ axes so that we can reverse this transformation later.
					</p>
					<p>
						The next few steps operate on one channel at a time, and the process for each channel is the same, so we will just focus on one
						channel to make it easier to understand. You can click on the channels below to change which channel is displayed in the following
						steps.
					</p>
					<GraphSet selectable={true}>
						<ImageGraph label="X Channel" image={data.decorrelated?.x} selected={channel === 'x'} onSelect={selectChannel('x')} />
						<ImageGraph label="Y Channel" image={data.decorrelated?.y} selected={channel === 'y'} onSelect={selectChannel('y')} />
						<ImageGraph label="Z Channel" image={data.decorrelated?.z} selected={channel === 'z'} onSelect={selectChannel('z')} />
					</GraphSet>
				</aside>
			</section> }
			{ data && <section>
				<aside>
					<h3>Channel Distribution</h3>
					<p>
						Now we have a channel that is mostly uncorrelated with the others, we can start doing frequency analysis on it. However, there is one
						more issue with the data we want to account for. Generally, the distribution of values in a channel will be biased and distorted in
						some way from a standard normal distribution. To be able to analyze each channel consistently, we first want to normalize the
						distribution of values in the channel to match a normal distribution.
					</p>
					<GraphSet>
						<ImageGraph label="Channel" image={data.decorrelated?.[channel]} />
						<ImageGraph label="Histogram" image={data.channels[channel].histogram} />
						<ImageGraph label="Cumulative Probability" image={data.channels[channel].probability} />
					</GraphSet>
				</aside>
			</section> }
			{ data && <section>
				<aside>
					<h3>Normalized Channel</h3>
					<p>
						After normalization, the distribution of values will roughly match a normal distribution. We will save the mapping between the old
						distribution and the new distribution so that we can reverse this transformation later.
					</p>
					<GraphSet>
						<ImageGraph label="Normalized Channel" image={data.channels[channel].normalized} />
						<ImageGraph label="Value Mapping" image={data.channels[channel].normalizationMap} />
						<ImageGraph label="Histogram" image={data.channels[channel].normalizedHistogram} />
						<ImageGraph label="Cumulative Probability" image={data.channels[channel].normalizedProbability} />
					</GraphSet>
				</aside>
			</section> }
			{ data && <section>
				<aside>
					<h3>Normalized Channel</h3>
					<p>
						Now that we have a normally distributed channel, we can analyze the frequency spectrum of the channel by running it through a&nbsp;
						<a href="https://en.wikipedia.org/wiki/Fourier_transform" target="_blank" rel="noreferrer">Fourier Transform</a>. The Fourier
						transform extracts the different continuous variation patterns at different levels of resolution so they can be analyzed separately.
					</p>
					<GraphSet>
						<ImageGraph label="Normalized Channel" image={data.channels[channel].normalized} />
						<ImageGraph label="Frequency Spectrum" image={data.channels[channel].frequency} />
					</GraphSet>
				</aside>
			</section> }
			{ data && <section>
				<aside>
					<h3>Frequency Analysis</h3>
					<p>
						We will split the Fourier transform data into eight different regions which we will analyze separately, you can see these different
						regions separated into colored bands on the spectral graph below. For each band, we will derive a weight value based on how much
						variation is present in that band, as displayed in the table below.
					</p>
					<GraphSet>
						<ImageGraph label="Normalized Channel" image={data.channels[channel].normalized} />
						<ImageGraph label="Frequency Spectrum" image={data.channels[channel].frequencyBands} />
						<Graph label="Band Weights">
							<table className={styles.threeColumn}>
								<thead>
									<tr>
										<th>Band</th>
										<th>Color</th>
										<th>Weight</th>
									</tr>
								</thead>
								<tbody>
									{ data.channels[channel].bandWeights?.map((weight, band) => <tr key={band}>
										<td>{band + 1}</td>
										<td><div className={styles.swatch} style={{background: weight.color}} /></td>
										<td>{weight.value.toFixed(3)}</td>
									</tr>) }
									{ !data.channels[channel].bandWeights && <tr>
										<td colSpan={3}>Loading...</td>
									</tr> }
								</tbody>
							</table>
						</Graph>
					</GraphSet>
				</aside>
			</section> }
			{ data && <section>
				<aside>
					<h3>Channel Fingerprint</h3>
					<p>
						These band weights and the distribution mapping makes up the &quot;fingerprint&quot; of this channel of the image. We can now use this
						to generate a new version of the channel from random noise that has similar properties as the original channel, which lets us
						construct a new, procedurally generated version of the image.
					</p>
					<GraphSet>
						<ImageGraph label="Value Mapping" image={data.channels[channel].normalizationMap} />
						<Graph label="Band Weights">
							<table className={styles.twoColumn}>
								<thead>
									<tr>
										<th>Band</th>
										<th>Weight</th>
									</tr>
								</thead>
								<tbody>
									{ data.channels[channel].bandWeights?.map((weight, index) => <tr key={index}>
										<td>{index + 1}</td>
										<td>{weight.value.toFixed(3)}</td>
									</tr>) }
									{ !data.channels[channel].bandWeights && <tr>
										<td colSpan={2}>Loading...</td>
									</tr> }
								</tbody>
							</table>
						</Graph>
					</GraphSet>
				</aside>
			</section> }
			{ data && <section>
				<aside>
					<h3>Noise Layers</h3>
					<p>
						First, we will generate the new channel by combining multiple layers of&nbsp;
						<a href="https://en.wikipedia.org/wiki/Wavelet_noise" target="_blank" rel="noreferrer">Wavelet Noise</a>. Wavelet noise has the useful
						property that it&apos;s frequency spectrum is mostly limited to a small band of variation. This allows us to combine multiple layers
						of it, one for each of the bands we analyzed earlier, and weight each layer by the band weights we saved previously.
					</p>
					{ range(NUMBER_OF_BANDS).map(band => <GraphSet key={band}>
						<ImageGraph label="Noise Layer" image={data.channels[channel].noiseLayers?.[band]} />
						<ImageGraph label="Frequency Spectrum" image={data.channels[channel].noiseLayerFrequencies?.[band]} />
						<Graph label="Band Weight">
							<div className={styles.multiplier}>
								&times; {data.channels[channel].bandWeights?.[band].value.toFixed(3)}
							</div>
						</Graph>
						<ImageGraph label="Weighted Spectrum" image={data.channels[channel].noiseLayerWeightedFrequencies?.[band]} />
					</GraphSet>) }
				</aside>
			</section> }
			{ data && <section>
				<aside>
					<h3>Noise Channel</h3>
					<p>
						Once we combine all these noise layers together, we get a signal that has a similar frequency spectrum to the original channel.
					</p>
					<GraphSet>
						<ImageGraph label="Noise Channel" image={data.channels[channel].noise} />
						<ImageGraph label="Normalized Channel" image={data.channels[channel].normalized} />
						<ImageGraph label="Noise Spectrum" image={data.channels[channel].noiseFrequency} />
						<ImageGraph label="Normalized Spectrum" image={data.channels[channel].frequency} />
					</GraphSet>
				</aside>
			</section> }
			{ data && <section>
				<aside>
					<h3>Denormalized Noise</h3>
					<p>
						Now we just need to use the mapping we saved previously for normalizing the distribution of values in the image to a normal
						distribution, but this time we will reverse that process to bias the generated image in the same way the original image was biased.
					</p>
					<GraphSet>
						<ImageGraph label="Denormalized Noise" image={data.channels[channel].denormalized} />
						<ImageGraph label="Original Channel" image={data.decorrelated?.[channel]} />
						<ImageGraph label="Noise Histogram" image={data.channels[channel].denormalizedHistogram} />
						<ImageGraph label="Original Histogram" image={data.channels[channel].histogram} />
					</GraphSet>
				</aside>
			</section> }
			{ data && <section>
				<aside>
					<h3>Noise Channels</h3>
					<p>
						We have now completed the generation of a single channel for the new image. We perform the same process for all three XYZ channels.
					</p>
					<GraphSet>
						<ImageGraph label="X Channel" image={data.channels.x.denormalized} />
						<ImageGraph label="Y Channel" image={data.channels.y.denormalized} />
						<ImageGraph label="Z Channel" image={data.channels.z.denormalized} />
					</GraphSet>
				</aside>
			</section> }
			{ data && <section>
				<aside>
					<h3>Noise Channels</h3>
					<p>
						Once we have all three of our newly generated channels, we can reverse the axis transformation we did during the PCA process to derive
						a normal set of red, green, and blue channels.
					</p>
					<GraphSet>
						<ImageGraph label="Red Channel" image={data.colors?.x} />
						<ImageGraph label="Green Channel" image={data.colors?.y} />
						<ImageGraph label="Blue Channel" image={data.colors?.z} />
					</GraphSet>
				</aside>
			</section> }
			{ data && <section>
				<aside>
					<h3>Generated Image</h3>
					<p>
						We combine these new RGB channels into a regular image, and we have a new, procedurally generated version of the original image! Once
						we&apos;ve derived the &quot;fingerprints&quot; of the channels, producing new procedurally generated versions is very easy, as it
						just involves generating some Wavelet Noise and doing some mappings on the channel data. You can click the &quot;Regenerate
						Noise&quot; button below to have it regenerate a new version of the image.
					</p>
					<p>
						<button disabled={!data.final} onClick={refreshImage}>Regenerate Noise</button>
					</p>
					<GraphSet>
						<ImageGraph label="Original Image" image={data.originalColor} />
						<ImageGraph label="Generated Image" image={data.final} />
					</GraphSet>
				</aside>
			</section> }
			{ data && <section>
				<aside>
					<h3>Thanks for reading!</h3>
					<p>
						You can find the original paper this implementation was based on <a href="http://graphics.cs.kuleuven.be/publications/LVLD10PISTE" target="_blank" rel="noreferrer">here</a>.
					</p>
					<p>
						You can find the code for this implementation on GitLab <a href="https://gitlab.com/zookatron/procedural-isotropic-textures" target="_blank" rel="noreferrer">here</a>.
					</p>
				</aside>
			</section> }
		</main>
	</>;
}

ReactDOM.render(<React.StrictMode><ErrorBoundary><App /></ErrorBoundary></React.StrictMode>, document.getElementById('container'));
