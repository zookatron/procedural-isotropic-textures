import {sum, sortBy} from 'lodash-es';
import Complex from '../math/complex';
import {FourierTransform2D, InverseFourierTransform2D} from '../math/fourier';
import WaveletNoise from '../math/wavelet';
import getEigenVectors from '../math/pca';
import erf from '../math/erf';

export type ColorMap = (value: number, x: number, y: number) => [number, number, number];

export class TriSignal2D {
	public size: number;
	public x: Signal2D;
	public y: Signal2D;
	public z: Signal2D;

	constructor(size: number) {
		this.size = size;
		this.x = new Signal2D(this.size);
		this.y = new Signal2D(this.size);
		this.z = new Signal2D(this.size);
	}

	public clone(): TriSignal2D {
		return new TriSignal2D(this.size).copy(this);
	}

	public copy(other: TriSignal2D): TriSignal2D {
		if (this.size !== other.size) {
			throw new Error('Cannot copy signals of different size!');
		}
		this.x.copy(other.x);
		this.y.copy(other.y);
		this.z.copy(other.z);
		return this;
	}

	public async load(image: ImageData): Promise<TriSignal2D> {
		for (let index = 0; index < this.size * this.size; index++) {
			this.x.values[index] = (image.data[index * 4 + 0] / 255) * 2 - 1;
			this.y.values[index] = (image.data[index * 4 + 1] / 255) * 2 - 1;
			this.z.values[index] = (image.data[index * 4 + 2] / 255) * 2 - 1;
		}
		return this;
	}

	public decorrelatedAxes(): number[][] {
		const matrix = [];
		for (let index = 0; index < this.size * this.size; index++) {
			matrix.push([this.x.values[index], this.y.values[index], this.z.values[index]]);
		}

		const {eigenvectors, eigenvalues} = getEigenVectors(matrix);
		return sortBy(eigenvectors, (vector, axis) => eigenvalues[axis]);
	}

	public average(): number[] {
		return [this.x.average(), this.y.average(), this.z.average()];
	}

	public rebasis(axes: number[][]): TriSignal2D {
		const x = this.x;
		const y = this.y;
		const z = this.z;
		const signals = [x, y, z];
		const values = [0, 0, 0];

		for (let index = 0; index < this.size * this.size; index++) {
			for (let signal = 0; signal < 3; signal++) {
				values[signal] = x.values[index] * axes[signal][0] + y.values[index] * axes[signal][1] + z.values[index] * axes[signal][2];
			}
			for (let signal = 0; signal < 3; signal++) {
				signals[signal].values[index] = values[signal];
			}
		}

		return this;
	}

	public debasis(axes: number[][]): TriSignal2D {
		const x = this.x;
		const y = this.y;
		const z = this.z;
		const signals = [x, y, z];
		const values = [0, 0, 0];

		for (let index = 0; index < this.size * this.size; index++) {
			for (let signal = 0; signal < 3; signal++) {
				values[signal] = 0;
			}
			for (let signal = 0; signal < 3; signal++) {
				values[0] += axes[signal][0] * signals[signal].values[index];
				values[1] += axes[signal][1] * signals[signal].values[index];
				values[2] += axes[signal][2] * signals[signal].values[index];
			}
			for (let signal = 0; signal < 3; signal++) {
				signals[signal].values[index] = values[signal];
			}
		}

		return this;
	}

	public render(color?: ColorMap, range: [number, number] = [-1, 1]) {
		const data = new Uint8ClampedArray(this.size * this.size * 4);
		const min = range[0];
		const max = range[1];
		const inverseRange = 1 / (max - min);

		for (let y = 0; y < this.size; y++) {
			for (let x = 0; x < this.size; x++) {
				const pixel = y * this.size + x;
				data[pixel * 4 + 0] = ((this.x.values[pixel] - min) * inverseRange) * 255;
				data[pixel * 4 + 1] = ((this.y.values[pixel] - min) * inverseRange) * 255;
				data[pixel * 4 + 2] = ((this.z.values[pixel] - min) * inverseRange) * 255;
				data[pixel * 4 + 3] = 255;
			}
		}

		return new ImageData(data, this.size, this.size);
	}

	public serialize() {
		return {
			size: this.size,
			x: this.x.serialize(),
			y: this.y.serialize(),
			z: this.z.serialize(),
		};
	}

	public deserialize(data: ReturnType<TriSignal2D['serialize']>) {
		this.size = data.size;
		this.x.deserialize(data.x);
		this.y.deserialize(data.y);
		this.z.deserialize(data.z);
		return this;
	}
}

export class Signal2D {
	public size: number;
	public values: number[];

	constructor(size: number) {
		this.size = size;
		this.values = new Array(this.size * this.size).fill(0);
	}

	public get(x: number, y: number): number {
		return this.values[this.size * y + x];
	}

	public set(x: number, y: number, value: number): Signal2D {
		this.values[this.size * y + x] = value;
		return this;
	}

	public clone(): Signal2D {
		return new Signal2D(this.size).copy(this);
	}

	public copy(other: Signal2D): Signal2D {
		if (this.size !== other.size) {
			this.size = other.size;
			this.values = new Array(this.size * this.size).fill(0);
		}
		for (let index = 0; index < this.size * this.size; index++) {
			this.values[index] = other.values[index];
		}
		return this;
	}

	public scale(scale: number): Signal2D {
		for (let index = 0; index < this.size * this.size; index++) {
			this.values[index] *= scale;
		}
		return this;
	}

	public average(): number {
		let average = 0;
		for (let index = 0; index < this.size * this.size; index++) {
			average += this.values[index];
		}
		average /= this.size * this.size;
		return average;
	}

	public min(): number {
		let min = Infinity;
		for (let index = 0; index < this.size * this.size; index++) {
			if (this.values[index] < min) {
				min = this.values[index];
			}
		}
		return min;
	}

	public max(): number {
		let max = -Infinity;
		for (let index = 0; index < this.size * this.size; index++) {
			if (this.values[index] > max) {
				max = this.values[index];
			}
		}
		return max;
	}

	public fill(value: number): Signal2D {
		for (let index = 0; index < this.size * this.size; index++) {
			this.values[index] = value;
		}
		return this;
	}

	public noise(scale: number, seed = 1): Signal2D {
		const noise = new WaveletNoise(this.size / scale, seed);
		let pixel = 0;
		for (let y = 0; y < this.size; y++) {
			for (let x = 0; x < this.size; x++) {
				this.values[pixel++] = noise.get(x / scale + 0.25, y / scale + 0.25);
			}
		}
		return this;
	}

	public correlation(other: Signal2D) {
		/*
		 * A note to anyone reading this:
		 *   I realize that this is a bad measurement of the correlation between two 2D signals. I wasn't
		 *   able to find any good algorithms for visualizing the correlated locations in two signals like
		 *   this that were anything close to something I could figure out how to implement. If you know
		 *   of a good way to display a correlation image like this I would very much like to know about it.
		 */
		const thisAverage = this.average();
		const thisRange = Math.max(Math.abs(this.min() - thisAverage), Math.abs(this.max() - thisAverage));
		const otherAverage = other.average();
		const otherRange = Math.max(Math.abs(other.min() - otherAverage), Math.abs(other.max() - otherAverage));
		for (let index = 0; index < this.size * this.size; index++) {
			const thisNormalized = (this.values[index] - thisAverage) / thisRange;
			const otherNormalized = (other.values[index] - otherAverage) / otherRange;
			this.values[index] = Math.max(1 - Math.abs(thisNormalized - otherNormalized), 0) * 2 - 1;
		}
	}

	public fromFrequency(frequency: Frequency2D): Signal2D {
		InverseFourierTransform2D(this.size, frequency.values, this.values);
		return this;
	}

	public bounds(): [ number, number ] {
		let min = Infinity;
		let max = -Infinity;
		for (let index = 0; index < this.size * this.size; index++) {
			const value = this.values[index];
			if (value < min) {
				min = value;
			}
			if (value > max) {
				max = value;
			}
		}
		return [min, max];
	}

	public normalize(): Signal2D {
		const [min, max] = this.bounds();
		const range = max === min ? 1 : max - min;
		for (let index = 0; index < this.size * this.size; index++) {
			this.values[index] = ((this.values[index] - min) / range) * 2 - 1;
		}
		return this;
	}

	public denormalize(min: number, max: number): Signal2D {
		for (let index = 0; index < this.size * this.size; index++) {
			this.values[index] = (this.values[index] * 0.5 + 0.5) * (max - min) + min;
		}
		return this;
	}

	public variance(): number {
		const average = sum(this.values) / (this.size * this.size);

		let squareddiffs = 0;
		for (let index = 0; index < this.size * this.size; index++) {
			const diff = this.values[index] - average;
			squareddiffs += diff * diff;
		}

		return squareddiffs / (this.size * this.size);
	}

	public map(mapping: Signal1D): Signal2D {
		const bins = mapping.size;

		for (let index = 0; index < this.size * this.size; index++) {
			const value = Math.floor((this.values[index] * 0.5 + 0.5) * bins);
			const clampedValue = Math.min(bins - 1, Math.max(0, value));
			const newValue = (mapping.values[clampedValue] / bins) * 2 - 1;
			this.values[index] = newValue;
		}

		return this;
	}

	public fromWeights(weights: number[], seed = 1): Signal2D {
		for (let y = 0; y < this.size; y++) {
			for (let x = 0; x < this.size; x++) {
				this.set(x, y, 0);
			}
		}
		let scale = 1;
		for (let weight = 0; weight < weights.length; weight++) {
			const noise = new WaveletNoise(this.size / scale, seed + weight);
			for (let y = 0; y < this.size; y++) {
				for (let x = 0; x < this.size; x++) {
					this.set(x, y, this.get(x, y) + weights[weight] * noise.get(x / scale + 0.25, y / scale + 0.25));
				}
			}
			scale *= 2;
		}
		return this;
	}

	public render(color?: ColorMap, range: [number, number] = [-1, 1]) {
		const data = new Uint8ClampedArray(this.size * this.size * 4);
		const min = range[0];
		const max = range[1];
		const inverseRange = 1 / (max - min);

		for (let y = 0; y < this.size; y++) {
			for (let x = 0; x < this.size; x++) {
				const pixel = y * this.size + x;
				const value = (this.values[pixel] - min) * inverseRange;
				const [r, g, b] = color ? color(value, x, y) : [value, value, value];

				data[pixel * 4 + 0] = r * 255;
				data[pixel * 4 + 1] = g * 255;
				data[pixel * 4 + 2] = b * 255;
				data[pixel * 4 + 3] = 255;
			}
		}

		return new ImageData(data, this.size, this.size);
	}

	public serialize() {
		return {
			size: this.size,
			values: this.values,
		};
	}

	public deserialize(data: ReturnType<Signal2D['serialize']>) {
		this.size = data.size;
		this.values = data.values.concat([]);
		return this;
	}
}

export class Frequency2D {
	public size: number;
	public values: Complex[];

	constructor(size: number) {
		this.size = size;
		this.values = new Array(this.size * this.size).fill(null).map(() => new Complex());
	}

	public fromSignal(signal: Signal2D): Frequency2D {
		FourierTransform2D(this.size, signal.values, this.values);
		return this;
	}

	public get(x: number, y: number): Complex {
		return this.values[this.size * y + x];
	}

	public set(x: number, y: number, value: Complex): Frequency2D {
		this.values[this.size * y + x] = value;
		return this;
	}

	public scale(scale: number): Frequency2D {
		for (let index = 0; index < this.size * this.size; index++) {
			this.values[index].scale(scale);
		}
		return this;
	}

	public clone(): Frequency2D {
		return new Frequency2D(this.size).copy(this);
	}

	public copy(other: Frequency2D): Frequency2D {
		if (this.size !== other.size) {
			this.size = other.size;
			this.values = new Array(this.size * this.size).fill(null).map(() => new Complex());
		}
		for (let index = 0; index < this.size * this.size; index++) {
			this.values[index].copy(other.values[index]);
		}
		return this;
	}

	public removeOutliers(zScore = 10) {
		const count = this.size * this.size;
		let stdev = 0;
		for (let index = 0; index < count; index++) {
			const magnitude = this.values[index].magnitude();
			stdev += magnitude * magnitude;
		}
		stdev = Math.sqrt(stdev / count);
		for (let index = 0; index < count; index++) {
			const magnitude = this.values[index].magnitude();
			if (magnitude > stdev * zScore) {
				this.values[index].scale(stdev * zScore / magnitude);
			}
		}
		return this;
	}

	public logscale(flatness = 50) {
		const count = this.size * this.size;
		const factor = (flatness * flatness) / count;
		for (let index = 0; index < count; index++) {
			const magnitude = this.values[index].magnitude();
			this.values[index].scale(Math.log(factor * magnitude + 1) / magnitude);
		}
		return this;
	}

	public bandWeight(scale: number): number {
		const min = 1/4/scale;
		const max = 1/2/scale;
		let sum = 0;
		for (let y = 0; y < this.size; y++) {
			for (let x = 0; x < this.size; x++) {
				const xDifference = Math.abs(x - this.size / 2) / this.size;
				const yDifference = Math.abs(y - this.size / 2) / this.size;
				const maxDifference = Math.max(xDifference, yDifference);
				if (maxDifference >= min && maxDifference < max) {
					const mag = this.get(x, y).magnitude();
					sum += mag*mag;
				}
			}
		}
		const power = Math.sqrt(sum) / (this.size * this.size);
		const simplexCorrection = 1.1;
		const simplexStandardDeviation = 0.3;
		return power * simplexCorrection / simplexStandardDeviation;
	}

	public bandWeights(bands: number): number[] {
		const weights = new Array(bands).fill(0);
		let scale = 1;
		for (let band = 0; band < bands; band++) {
			weights[band] = this.bandWeight(scale);
			scale *= 2;
		}
		return weights;
	}

	public render(color?: ColorMap) {
		const data = new Uint8ClampedArray(this.size * this.size * 4);

		let maxMagnitude = 0;
		const count = this.size * this.size;
		for (let index = 0; index < count; index++) {
			const magnitude = this.values[index].magnitudeSquared();
			if (magnitude > maxMagnitude) {
				maxMagnitude = magnitude;
			}
		}
		maxMagnitude = Math.sqrt(maxMagnitude);

		for (let y = 0; y < this.size; y++) {
			for (let x = 0; x < this.size; x++) {
				const pixel = y * this.size + x;
				const value = this.values[pixel].magnitude() / maxMagnitude;
				const [r, g, b] = color ? color(value, x, y) : [value, value, value];

				data[pixel * 4 + 0] = r * 255;
				data[pixel * 4 + 1] = g * 255;
				data[pixel * 4 + 2] = b * 255;
				data[pixel * 4 + 3] = 255;
			}
		}

		return new ImageData(data, this.size, this.size);
	}

	public serialize() {
		return {
			size: this.size,
			values: this.values.map(value => value.serialize()),
		};
	}

	public deserialize(data: ReturnType<Frequency2D['serialize']>) {
		this.size = data.size;
		this.values = data.values.map(value => new Complex().deserialize(value));
		return this;
	}
}

export class Signal1D {
	public size: number;
	public values: number[];

	constructor(size: number) {
		this.size = size;
		this.values = new Array(this.size).fill(0);
	}

	public regularHistogram(bins: number, variance: number): Signal1D {
		return this.fromFunction(x => 1/Math.sqrt(2*Math.PI*variance) * Math.exp(-x*x/(2*variance)));
	}

	public regularProbability(bins: number, variance: number): Signal1D {
		return this.fromFunction(x => erf(x/(Math.sqrt(variance*2)))*0.5 + 0.5);
	}

	public fromFunction(func: (x: number) => number): Signal1D {
		for (let value = 0; value < this.size; value++) {
			this.values[value] = func(2 * value / this.size - 1);
		}
		return this;
	}

	public histogram(signal: Signal2D): Signal1D {
		this.cumulativeProbability(signal);

		let lastProbability = 0;
		for (let bin = 0; bin < this.size; bin++) {
			const difference = this.values[bin] - lastProbability;
			lastProbability = this.values[bin];
			this.values[bin] = difference * this.size * 0.5;
		}

		return this;
	}

	public cumulativeProbability(signal: Signal2D): Signal1D {
		this.values.fill(0);

		for (let index = 0; index < signal.size * signal.size; index++) {
			const bin = Math.floor((signal.values[index] * 0.5 + 0.5) * this.size);
			const clampedBin = Math.min(this.size - 1, Math.max(0, bin));
			this.values[clampedBin] += 1;
		}

		let count = 0;
		for (let bin = 0; bin < this.size; bin++) {
			count += this.values[bin];
			this.values[bin] = count;
		}

		const factor = 1 / (signal.size * signal.size);
		for (let bin = 0; bin < this.size; bin++) {
			this.values[bin] *= factor;
		}

		return this;
	}

	public histogramMatch(source: Signal1D, dest: Signal1D): Signal1D {
		let sourceValue = 0;
		for (let value = 0; value < this.size; value++) {
			// eslint-disable-next-line no-constant-condition
			while (true) {
				if (sourceValue === this.size - 1 || source.values[sourceValue] >= dest.values[value]) {
					this.values[value] = sourceValue;
					break;
				}
				sourceValue++;
			}
		}

		return this;
	}

	public smooth(amount: number): Signal1D {
		const smoothed = new Array(this.size).fill(0);
		const reach = Math.ceil(3 * amount);
		const weights = new Array(2 * reach + 1).fill(0);

		for (let offset = -reach; offset <= reach; offset++) {
			weights[reach + offset] = 1/Math.sqrt(2*Math.PI*amount*amount) * Math.exp(-offset*offset/(2*amount*amount));
		}

		for (let value = 0; value < this.size; value++) {
			for (let offset = -reach; offset <= reach; offset++) {
				let position = value + offset;
				if (position < 0) {
					position = 0;
				}
				if (position >= this.size) {
					position = this.size - 1;
				}
				smoothed[value] += weights[reach + offset] * this.values[position];
			}
		}

		for (let value = 0; value < this.size; value++) {
			this.values[value] = smoothed[value];
		}

		return this;
	}

	public render(yScale = 1) {
		const data = new Uint8ClampedArray(this.size * this.size * 4);

		for (let x = 0; x < this.size; x++) {
			const functionValue = this.size - Math.floor(this.values[x] / yScale * this.size);
			for (let y = 0; y < this.size; y++) {
				const pixel = y * this.size + x;
				const value = y >= functionValue ? 1 : 0;
				data[pixel * 4 + 0] = value * 255;
				data[pixel * 4 + 1] = value * 255;
				data[pixel * 4 + 2] = value * 255;
				data[pixel * 4 + 3] = 255;
			}
		}

		return new ImageData(data, this.size, this.size);
	}

	public serialize() {
		return {
			size: this.size,
			values: this.values,
		};
	}

	public deserialize(data: ReturnType<Signal1D['serialize']>) {
		this.size = data.size;
		this.values = data.values;
		return this;
	}
}
