import {mapValues, range} from 'lodash-es';
import {TriSignal2D, Signal2D, Frequency2D, Signal1D, ColorMap} from './library/signal';
import {NUMBER_OF_BANDS, SIZE, SIMPLEX_VARIANCE} from './common/constants';
import {averageImages, Timer} from './common/utilities';

const channelColors = mapValues({
	x: [1, 0, 0],
	y: [0, 1, 0],
	z: [0, 0, 1],
}, color => (value => [value * color[0], value * color[1], value * color[2]]) as ColorMap);

type ChannelName = 'x' | 'y' | 'z';
const mapChannels = <Type>(func: (channel: ChannelName) => Type) => mapValues({x: null, y: null, z: null}, (_, channel) => func(channel as ChannelName));

const bands = range(NUMBER_OF_BANDS).map(band => ({index: band, min: 1/4/Math.pow(2, band), max: 1/2/Math.pow(2, band)}));
const bandColors: Array<[number, number, number]> = [
	[1, 0, 0],
	[0, 1, 0],
	[0, 0, 1],
	[1, 1, 0],
	[0, 1, 1],
	[1, 0, 1],
	[1, 0.5, 0],
	[0, 1, 0.5],
	[0.5, 0, 1],
];
const frequencyBands: ColorMap = (value, x, y) => {
	const xDiff = Math.abs(x - SIZE / 2) / SIZE;
	const yDiff = Math.abs(y - SIZE / 2) / SIZE;
	const maxDiff = Math.max(xDiff, yDiff);
	const band = bands.find(eachBand => maxDiff >= eachBand.min && maxDiff < eachBand.max);
	const color = band ? bandColors[band.index] : [0, 0, 0];
	return [value * 0.75 + color[0] * 0.25, value * 0.75 + color[1] * 0.25, value * 0.75 + color[2] * 0.25];
};
const weightedColor = (weight: number): ColorMap => value => [weight * value, weight * value, weight * value];

const worker = self as unknown as Worker;
worker.addEventListener('message', async ({data: {image}}) => {
	try {
		console.log('start');
		const timer = new Timer();
		const seeds = mapChannels(() => Math.random() * 1000000);

		timer.start();
		const original = await new TriSignal2D(SIZE).load(image);
		const originalPixels = mapChannels(() => [] as number[]);
		const step = Math.max(Math.floor(original.size * original.size / 1000), 1);
		for (let pixel = 0; pixel < 5000; pixel++) {
			originalPixels.x[pixel] = original.x.values[pixel * step] * 0.5 + 0.5;
			originalPixels.y[pixel] = original.y.values[pixel * step] * 0.5 + 0.5;
			originalPixels.z[pixel] = original.z.values[pixel * step] * 0.5 + 0.5;
		}
		worker.postMessage({
			original: mapChannels(channel => original[channel].render(channelColors[channel])),
			originalPixels,
		});
		timer.end('original');

		timer.start();
		const correlation = original.clone();
		correlation.x.correlation(original.y);
		correlation.y.correlation(original.z);
		correlation.z.correlation(original.x);
		worker.postMessage({correlation: mapChannels(channel => correlation[channel].render())});
		timer.end('correlation');

		timer.start();
		const axes = original.decorrelatedAxes();
		const center = original.average();
		timer.end('decorrelatedAxes');
		timer.start();
		const decorrelated = original.clone().rebasis(axes);
		const decorrelatedBounds = mapChannels(channel => {
			const decorrelatedChannel = decorrelated[channel];
			const bounds = decorrelatedChannel.bounds();
			decorrelatedChannel.normalize();
			return bounds;
		});
		worker.postMessage({
			decorrelated: mapChannels(channel => decorrelated[channel].render()),
			decorrelatedAxes: axes,
			decorrelatedAxesCenter: center,
		});
		timer.end('decorrelated');

		timer.start();
		const regularHistogram = new Signal1D(SIZE).regularHistogram(SIZE, SIMPLEX_VARIANCE);
		const regularHistogramRender = regularHistogram.render(2);
		const regularProbability = new Signal1D(SIZE).regularProbability(SIZE, SIMPLEX_VARIANCE);
		const regularProbabilityRender = regularProbability.render();
		timer.end('regularHistograms');

		const channels = mapChannels(channel => {
			const seed = seeds[channel];
			const decorrelatedChannel = decorrelated[channel];

			timer.start();
			const histogram = new Signal1D(SIZE).histogram(decorrelatedChannel).smooth(2);
			worker.postMessage({[`channels.${channel}.histogram`]: averageImages([histogram.render(2), regularHistogramRender])});
			timer.end(`${channel} histogram`);

			timer.start();
			const probability = new Signal1D(SIZE).cumulativeProbability(decorrelatedChannel);
			worker.postMessage({[`channels.${channel}.probability`]: averageImages([probability.render(), regularProbabilityRender])});
			timer.end(`${channel} probability`);

			timer.start();
			const normalizationMap = new Signal1D(SIZE).histogramMatch(regularProbability, probability);
			const denormalizationMap = new Signal1D(SIZE).histogramMatch(probability, regularProbability);
			worker.postMessage({[`channels.${channel}.normalizationMap`]: normalizationMap.render(SIZE)});
			timer.end(`${channel} normalizationMap`);

			timer.start();
			const normalized = decorrelatedChannel.clone().map(normalizationMap);
			worker.postMessage({[`channels.${channel}.normalized`]: normalized.render()});
			timer.end(`${channel} normalized`);

			timer.start();
			const normalizedHistogram = new Signal1D(SIZE).histogram(normalized).smooth(2);
			worker.postMessage({[`channels.${channel}.normalizedHistogram`]: averageImages([normalizedHistogram.render(2), regularHistogramRender])});
			timer.end(`${channel} normalizedHistogram`);

			timer.start();
			const normalizedProbability = new Signal1D(SIZE).cumulativeProbability(normalized);
			worker.postMessage({[`channels.${channel}.normalizedProbability`]: averageImages([normalizedProbability.render(), regularProbabilityRender])});
			timer.end(`${channel} normalizedProbability`);

			timer.start();
			const frequency = new Frequency2D(SIZE).fromSignal(normalized);
			const bandWeights = frequency.bandWeights(NUMBER_OF_BANDS);
			frequency.removeOutliers().logscale();
			worker.postMessage({
				[`channels.${channel}.frequency`]: frequency.render(),
				[`channels.${channel}.frequencyBands`]: frequency.render(frequencyBands),
				[`channels.${channel}.bandWeights`]: bandWeights.map((value, band) => ({
					value,
					color: `rgb(${bandColors[band][0] * 255}, ${bandColors[band][1] * 255}, ${bandColors[band][2] * 255})`,
				})),
			});
			timer.end(`${channel} frequency`);

			timer.start();
			const noiseLayers = range(NUMBER_OF_BANDS).map(band => new Signal2D(SIZE).noise(Math.pow(2, band), seed + band));
			worker.postMessage({[`channels.${channel}.noiseLayers`]: noiseLayers.map(layer => layer.render())});
			timer.end(`${channel} noiseLayers`);

			timer.start();
			const noise = new Signal2D(SIZE).fromWeights(bandWeights, seed);
			worker.postMessage({[`channels.${channel}.noise`]: noise.render()});
			timer.end(`${channel} noise`);

			timer.start();
			const denormalized = noise.clone().map(denormalizationMap);
			const denormalizedHistogram = new Signal1D(SIZE).histogram(denormalized).smooth(2);
			worker.postMessage({
				[`channels.${channel}.denormalized`]: denormalized.render(),
				[`channels.${channel}.denormalizedHistogram`]: averageImages([denormalizedHistogram.render(2), regularHistogramRender]),
			});
			timer.end(`${channel} channel`);

			return {
				bandWeights,
				noiseLayers,
				noise,
				denormalized,
			};
		});

		timer.start();
		const colors = new TriSignal2D(SIZE);
		mapChannels(channel => colors[channel].copy(channels[channel].denormalized).denormalize(...decorrelatedBounds[channel]));
		colors.debasis(axes);
		worker.postMessage({
			colors: mapChannels(channel => colors[channel].render(channelColors[channel])),
			final: colors.render(),
		});
		timer.end('colors');

		mapChannels(channel => {
			timer.start();
			const bandWeights = channels[channel].bandWeights;
			const noiseLayers = channels[channel].noiseLayers;
			const noiseLayerFrequencies = noiseLayers.map(layer => new Frequency2D(SIZE).fromSignal(layer).removeOutliers().logscale());
			const noiseLayerFrequencyImages = noiseLayerFrequencies.map(layer => layer.render());
			const noiseLayerFrequencyWeightedImages = noiseLayerFrequencies.map((layer, band) => layer.render(weightedColor(bandWeights[band])));
			worker.postMessage({
				[`channels.${channel}.noiseLayerFrequencies`]: noiseLayerFrequencyImages,
				[`channels.${channel}.noiseLayerWeightedFrequencies`]: noiseLayerFrequencyWeightedImages,
			});
			timer.end(`${channel} noiseLayerFrequencies`);

			timer.start();
			const noise = channels[channel].noise;
			const noiseFrequency = new Frequency2D(SIZE).fromSignal(noise);
			worker.postMessage({[`channels.${channel}.noiseFrequency`]: noiseFrequency.removeOutliers().logscale().render()});
			timer.end(`${channel} noise frequency`);
		});
	} catch (error) {
		console.error(error);
	}
});
