const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');

module.exports = {
	entry: {
		index: './src/index.tsx',
	},
	mode: process.env.NODE_ENV || 'development',
	devtool: process.env.NODE_ENV === 'production' ? 'source-map' : 'cheap-source-map',
	resolve: {
		extensions: [ '.tsx', '.ts', '.js', '.css', '.png' ],
	},
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				exclude: /node_modules/,
				use: 'ts-loader',
			},
			{
				test: /\.css$/,
				use: [
					'style-loader',
					{ loader: 'css-loader', options: { modules: { auto: /.*/, exportLocalsConvention: 'camelCaseOnly' } } },
					{ loader: 'postcss-loader', options: {
						ident: 'postcss',
						plugins: (loader) => [
							require('postcss-import')({ root: loader.resourcePath }),
							require('postcss-preset-env')({ stage: 1 }),
							require('postcss-custom-properties')({ preserve: false }),
						],
					} },
				],
			},
			{
				test: /\.(png|jpe?g|gif)$/i,
				use: [
					{
						loader: 'file-loader',
					},
				],
			},
		],
	},
	plugins: [
		new HtmlWebpackPlugin({ template: 'src/index.html', chunks: ['index'], filename: 'index.html' }),
    	new FaviconsWebpackPlugin({ logo: path.resolve(__dirname, 'src/images/logo.png') }),
	],
	output: {
		filename: '[chunkhash].js',
		path: path.resolve(__dirname, 'public'),
		publicPath: process.env.PUBLIC_PATH || '/',
	},
	devServer: {
		watchOptions: {
			poll: true,
		},
	},
};
